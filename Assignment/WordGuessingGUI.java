import java.awt.Graphics;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.*;
import javax.swing.*;

public class WordGuessingGUI {
  private int WIDTH = 250;
  private int HEIGHT = 216;
  
  private JFrame frame;
  private JPanel panelMain;
  private JPanel panel;
  private JPanel panel2;
  private JPanel panel3;
  private JPanel panel4;
  private JPanel panel5;
  private JPanel panel6;
  private JLabel letterInArea, wordLab, wordDis, guessDis, letterCount, guessCount, noGuess;
  private JTextField letterInput, guesses;
  private JComboBox<String> numLetters;
  private JButton start, guess; 
  
  private Game wordGuessing;
  
  public WordGuessingGUI() {
    frame = new JFrame("Word Guessing Game");
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    
    letterInArea = new JLabel("Enter a Letter:");
    wordLab = new JLabel("Current Word: ");//updates while game is played.
    wordDis = new JLabel("");
    guessDis = new JLabel("Guessed Letters: ");
    letterCount = new JLabel("Select word size:");
    guessCount = new JLabel("Guess Count: --");
    noGuess = new JLabel("No. of Guesses: ");
    
    letterInput = new JTextField(1);
    letterInput.addActionListener(new GuessLetterListener());
    letterInput.setEnabled(false);
    
    guesses = new JTextField(2);
    
    //Combobox which contains the best word lengths
    String[] wordLengths = new String[11];
    for(int i = 0; i < wordLengths.length; i++) {
      wordLengths[i] =  i + 2 + "";
    }
    numLetters = new JComboBox<String>(wordLengths);
    
    
    start = new JButton("Start");
    start.addActionListener(new StartGameListener());
    guess = new JButton("Guess");
    guess.addActionListener(new GuessLetterListener());
    guess.setEnabled(false);
    
    panelMain = new JPanel(new GridLayout(6,1));
    panelMain.setPreferredSize(new Dimension(WIDTH, HEIGHT));
    panel = new JPanel();
    panel.setBackground(Color.white);
    panel2 = new JPanel();
    panel2.setBackground(Color.white);
    panel3 = new JPanel();
    panel3.setBackground(Color.white);
    panel4 = new JPanel();
    panel4.setBackground(Color.white);
    panel5 = new JPanel();
    panel5.setBackground(Color.white);
    panel6 = new JPanel();
    panel6.setBackground(Color.white);
    
    panel.add(letterCount);
    panel.add(numLetters);//adds the combo box for number of letters
    
    panel.add(guesses);
    
    panel2.add(start);//adds the start button
    //pressing start should get whats in the combo box
    
    panel3.add(wordLab);
    panel3.add(wordDis);
    
    panel4.add(guessDis);
    
    panel5.add(letterInArea);
    panel5.add(letterInput);
    panel5.add(guessCount);
    
    panel6.add(guess);
    
    panelMain.add(panel);
    panelMain.add(panel2);
    panelMain.add(panel3);
    panelMain.add(panel4);
    panelMain.add(panel5);
    panelMain.add(panel6);
    
    frame.getContentPane().add(panelMain);
    
    //Game class
    wordGuessing = new Game();
  }
  
  public void display() {
    frame.pack();
    frame.setVisible(true);
  }
  
  private class StartGameListener implements ActionListener {
    public void actionPerformed(ActionEvent event) {
      if(!wordGuessing.isRunning()) {
        int wordLength = Integer.parseInt(numLetters.getSelectedItem().toString());
        wordGuessing.setWordLength(wordLength);
        wordGuessing.startGame();
        numLetters.setEnabled(false);
        guess.setEnabled(true);
        letterInput.setEnabled(true);
        
        String guessNum = guesses.getText();
        //24a
        if (guessNum.matches("[0-9]+")) {
          wordGuessing.setTotalGuesses(Integer.parseInt(guessNum));
        }else {
          guesses.setText(wordGuessing.remainingGuesses() + "");
        }
        guesses.setEnabled(false);
        
        start.setText("Stop");
        guessCount.setText("Guess Count: " + wordGuessing.remainingGuesses());
        wordDis.setText(wordGuessing.getWord());
      }else {
        wordGuessing.stopGame();
        numLetters.setEnabled(true);
        guess.setEnabled(false);
        letterInput.setEnabled(false);
        guesses.setEnabled(true);
        guesses.setText("");
        
        start.setText("Start");
        wordDis.setText("");
        guessDis.setText("Guessed Letters: ");
        guessCount.setText("Guess Count: --");
        wordLab.setText("Current Word: ");
      }
    }
  }
  
  private class GuessLetterListener implements ActionListener {
    public void actionPerformed(ActionEvent event) {
      if(wordGuessing.remainingGuesses() > 0 && wordGuessing.getWord().indexOf("-") != -1) {
        String guessedLetter = letterInput.getText();
        wordGuessing.guess(guessedLetter.toLowerCase());
        //prints the guesses
        guessDis.setText("Guessed Letters: " + wordGuessing.getGuesses());
        //prints the word
        wordDis.setText(wordGuessing.getWord());
        wordLab.setText("Current Word: ");
        letterInput.setText("");
        guessCount.setText("Guess Count: " + wordGuessing.remainingGuesses());
      }
      if (wordGuessing.remainingGuesses() == 0 && wordGuessing.getWord().indexOf("-") != -1) {
        //Prints what the word was
        wordLab.setText("The word was: ");
        wordDis.setText(wordGuessing.getRandWord());
        letterInput.setText("");
        guessCount.setText("Guess Count: " + wordGuessing.remainingGuesses());
        guess.setEnabled(false);
        letterInput.setEnabled(false);
      }
      if (wordGuessing.getWord().indexOf("-") == -1) {
        //Says you have won
        wordLab.setText("You Win! ");
        guessCount.setText("Guess Count: " + wordGuessing.remainingGuesses());
        letterInput.setText("");
        guess.setEnabled(false);
        letterInput.setEnabled(false);
      }
    }
  }
}





