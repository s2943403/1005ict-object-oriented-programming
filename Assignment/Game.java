import java.util.*;

public class Game
{
  private int wordLength;
  private Dictionary words;
  private int totalGuesses;
  private int guesses;
  private String[] word;
  private boolean gameStarted;
  
  //will initialise the game as not running
  //call setWordLength(int wordLength) to open the dictionary and set other values
  public Game() {
    gameStarted = false;
  }
  
  //initialises the game and opens the dictionary
  public Game(int wrdLnth) {
    wordLength = wrdLnth;
    words = new Dictionary("dictionary.txt", wordLength);
    totalGuesses = 3*wordLength;
    totalGuesses = (totalGuesses > 15) ? 15: totalGuesses;
    guesses = 0;
    word = new String[wordLength];
    for(int i = 0; i < wordLength; i++) {
      word[i] = "-";
    }
    gameStarted = false;
  }
  
  //Will start the game
  public void startGame() {
    if(!words.open()) {
      System.out.println("File could not be found.");
      return;
    }else {
      gameStarted = true;
    }
  }
  
  //Will stop the game, resetting all values
  public void stopGame() {
    words.setList(new Vector<String>());
    wordLength = 0;
    totalGuesses = 0;
    guesses = 0;
    word = new String[0];
    gameStarted = false;
  }
  
  //returns whether a game is running or not
  public boolean isRunning() {
    return gameStarted;
  }
  
  //validates any guess being made before adding it to the guesses string
  public void guess(String letter) {
    if(words.validateInput(letter)) {
      words.addGuess(letter.toLowerCase());
      words.setList(catagory(letter.toLowerCase()));
      guesses++;
    }
  }
  
  //selects a random word from the vector and returns it
  public String getRandWord() {
    return words.selectWord();
  }
  
  //returns the word that is being guessed (filled with dashes for blanks)
  public String getWord() {
    StringBuilder b = new StringBuilder();
    for(String s : word) {
      b.append(s);
    }
    return b.toString();
  }
  
  //returns the guesses that have been made
  public String getGuesses() {
    return words.getGuesses();
  }
  
  //sets the word size
  public void setWordLength(int wordLngth) {
    wordLength = wordLngth;
    words = new Dictionary("dictionary.txt", wordLength);
    totalGuesses = 3*wordLength;
    totalGuesses = (totalGuesses > 15) ? 15: totalGuesses;
    guesses = 0;
    word = new String[wordLength];
    for(int i = 0; i < wordLength; i++) {
      word[i] = "-";
    }
    gameStarted = true;
  }
  
  //overrides the default number of guesses in the game.
  public void setTotalGuesses(int newTotal) {
    totalGuesses = newTotal;
  }
  
  //returns how many guesses are remaining
  public int remainingGuesses() {
    return totalGuesses - guesses;
  }
  
  //Sorts the words into categories based on the letter positioning and returns the largest categorised vector
  private Vector<String> catagory(String letter) {
    Vector<Vector<String>> catagories = new Vector<Vector<String>>();
    int largestCat = 0;
    int letterIndex = 0;
    int largestIndex = 0;
    
    for(int i = 0; i <= wordLength; i++) {
      catagories.add(new Vector<String>());
    }
    for(int i = 0; i < words.getWordCount(); i++) {
      letterIndex = words.getList().get(i).indexOf(letter) + 1;
      catagories.get(letterIndex).add(words.getList().get(i));
    }
    for(int i = 0; i < catagories.size(); i++) {
      if(catagories.get(i).size() > largestCat) {
        largestCat = catagories.get(i).size();
        largestIndex = i;
      }
    }
    if(largestIndex != 0)
      word[largestIndex-1] = letter;
    return catagories.get(largestIndex);
  }
}