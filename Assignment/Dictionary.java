import java.util.*;
import java.io.*;

public class Dictionary
{
  private Scanner file;
  private int wordLength;
  private Vector<String> dictionary;
  private String guesses;
  private String fileName;
  
  public Dictionary(String fileName, int wordLength)
  {
    this.wordLength = wordLength;
    this.fileName = fileName;
    dictionary = new Vector<String>();
  }
  
  //Will open the file and input the words into the vector,
  //returns false if the file cannot be found.
  public boolean open()
  {
    try 
    {
      file = new Scanner(new BufferedReader (new FileReader(fileName)));
      while(file.hasNext()){
        String temp = file.next();
        boolean letterDup = false;
        if(temp.length() == wordLength) {
          for (int i = 0; i < wordLength; i++) {
            if (temp.indexOf(temp.charAt(i)) != temp.lastIndexOf(temp.charAt(i))) {
              letterDup = true;
            }
          }
          if(!letterDup) {
            dictionary.add(temp);
          }
        }
      }
      return true;
    } catch(IOException e)
    {
      return false;
    } finally 
    {
      if(file != null)
        file.close();
      guesses = "";
    }
  }
  
  //returns the dictionary list to its caller
  public Vector<String> getList() {
    return dictionary;
  }
  //overrides the current list with the new list
  public void setList(Vector<String> newVector) {
    dictionary = newVector;
  }
  
  //Returns the amount of words in the dictionary.
  public int getWordCount() 
  {
    return dictionary.size();
  }
  
  //Randomly selects a word from the current list of words in the dictionary
  //used for when a game is lost
  public String selectWord()
  {
    String selectedWord = dictionary.get((int)(Math.random() * dictionary.size()));
    return selectedWord;
  }
  
  //Will determine if the input is a single character between a and z.
  public boolean validateInput(String guess) 
  {
    if(guess.length() > 1 || guess.length() == 0)
    {
      return false;
    } else
    {
      char c = guess.charAt(0);
      if(Character.toLowerCase(c) < 'a' || Character.toLowerCase(c) > 'z') {
        return false;
      }else if(guesses.indexOf(guess) == -1) {
        return true;
      }else {
        return false;
      }
    }
  }
  public void addGuess(String guess)
  {
    guesses += guess;
  }
  public String getGuesses() {
    return guesses;
  }
}